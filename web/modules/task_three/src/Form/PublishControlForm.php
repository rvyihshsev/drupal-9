<?php

namespace Drupal\task_three\Form;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure Task three settings for this site.
 */
class PublishControlForm extends FormBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a PublishControlForm object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'publish_control';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $articles = $this->entityTypeManager->getStorage('node')
      ->loadByProperties(['type' => 'article']);

    $articles_list = [];
    foreach ($articles as $article) {
      $articles_list[$article->id()] = $article->label();
    }

    $form['node'] = [
      '#type' => 'select',
      '#title' => $this->t('Node title'),
      '#options' => $articles_list
    ];

    $form['published'] = [
      '#type' => 'select',
      '#title' => $this->t('Published'),
      '#options' => [
        1 => 'Published',
        0 => 'Unpublished'
      ],
    ];
    $form['sticky'] = [
      '#type' => 'select',
      '#title' => $this->t('Sticky'),
      '#options' => [
        1 => 'Sticky',
        0 => 'Not sticky'
      ],
    ];

    $form['actions']['#type'] = 'actions';
    $form['actions']['update'] = [
      '#type' => 'submit',
      '#value' => t('Update'),
      '#submit' => [[$this, 'submitUpdate']],
    ];
    $form['actions']['delete'] = [
      '#type' => 'submit',
      '#value' => t('Delete'),
      '#submit' => [[$this, 'submitDelete']],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) { }

  /**
   * {@inheritdoc}
   */
  public function submitUpdate(array &$form, FormStateInterface $form_state)
  {
    $article = $this->entityTypeManager->getStorage('node')
      ->load($form_state->getValue('node'));

    $article->set('status', $form_state->getValue('published'))
      ->set('sticky', $form_state->getValue('sticky'))
      ->save();

    $this->messenger()->addStatus($this->t('Article %article updated.', ['%article' => $article->label()]));
  }

  /**
   * {@inheritdoc}
   */
  public function submitDelete(array &$form, FormStateInterface $form_state)
  {
    $article = $this->entityTypeManager->getStorage('node')
      ->load($form_state->getValue('node'));

    $article->delete();
    $this->messenger()->addStatus($this->t('Article %article deleted.', ['%article' => $article->label()]));
  }

}
