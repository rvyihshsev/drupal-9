<?php

namespace Drupal\random_quote\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\random_quote\RandomQuotes;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a random quotes block.
 *
 * @Block(
 *   id = "random_quotes_block",
 *   admin_label = @Translation("Random quotes"),
 *   category = @Translation("Custom")
 * )
 */
class RandomQuotesBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The random_quote.service service.
   *
   * @var \Drupal\random_quote\RandomQuotes
   */
  protected $randomQuoteService;

  /**
   * Constructs a new RandomQuotesBlock instance.
   *
   * @param array $configuration
   *   The plugin configuration, i.e. an array with configuration values keyed
   *   by configuration option name. The special key 'context' may be used to
   *   initialize the defined contexts by setting it to an array of context
   *   values keyed by context names.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\random_quote\RandomQuotes $random_quote_service
   *   The random_quote.random_quotes service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, RandomQuotes $random_quote_service) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->randomQuoteService = $random_quote_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('random_quote.service')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $random_quote = $this->randomQuoteService->getQuote();

    $build['content'] = [
      '#markup' => $random_quote,
    ];
    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    return 0;
  }

}
