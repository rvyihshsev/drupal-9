<?php

namespace Drupal\random_quote;

/**
 * RandomQuotes service.
 */
class RandomQuotes {

  /**
   * Returns random quote from quotable.io.
   */
  public function getQuote() {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, 'https://api.quotable.io/random');
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_TIMEOUT, 60);
    $response = curl_exec($ch);
    curl_close($ch);

    return json_decode($response)->content;
  }

}
